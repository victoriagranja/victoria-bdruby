module Docentes
    class Professor
        attr_accessor :salario, :nome, :matricula, :idade, :departamento

        def initialize salario, nome, matricula,idade, departamento
            @salario = salario
            @nome = nome
            @matricula = matricula
            @idade = idade
            @departamento = departamento
        end

        def salvar
            {salario: @salario, nome: @nome, matricula: @matricula, idade: @idade, departamento: @departamento}
        end
    end

    class Departamento
        attr_accessor :materias, :predio, :sigla

        def initialize materias, predio, sigla
            @materias = materias
            @predio = predio
            @sigla = sigla
        end
    end
end