module Discente
    class Aluno
        attr_accessor :nome, :matricula, :periodo, :curso
    end

    class AlunoGraduacao
        attr_accessor :cr, :carga_horaria

        def initialize cr, carga_horaria, nome, matricula, periodo, curso
            @cr = cr
            @carga_horaria = carga_horaria
            @nome = nome
            @matricula = matricula 
            @periodo = periodo
            @curso = curso
        end

        def salvar
            {cr: @cr, carga_horaria: @carga_horaria, nome: @nome, matricula: @matricula, periodo: @periodo, curso: @curso}
        end
    end

    class AlunoMestrado
        attr_accessor :graduacao, :orientador, :area_pesquisa

        def initialize graduacao, orientador, area_pesquisa, nome, matricula, periodo, curso
            @graduacao = graduacao
            @orientador = orientador
            @area_pesquisa = area_pesquisa
            @nome = nome
            @matricula = matricula 
            @periodo = periodo
            @curso = curso
        end

        def salvar
            {graduacao: @graduacao, orientador: @orientador, area_pesquisa: @area_pesquisa, nome: @nome, matricula: @matricula, periodo: @periodo, curso: @curso}
        end
    end
end
