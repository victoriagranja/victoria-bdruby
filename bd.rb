module BD
    class BD
        attr_accessor :tabela
        
        def initialize
            @tabela = Array.new
        end

        def salvar obj
            @tabela.push(obj.salvar)
        end
    end
end
