require_relative "docentes"
require_relative "discentes"
require_relative "bd"

include Docentes
GMA = Docentes::Departamento.new ["Calculos", "Geometria"], "UFASA", "GMA"
GAN = Docentes::Departamento.new ["Discreta", "Algebra", "Probabilidade"], "Bloco de Fisica", "GAN"
TCC = Docentes::Departamento.new ["Progs", "Estrutura de Dados", "Banco de Dados"], "IC", "TCC"

Rossetti = Docentes::Professor.new 7500.52, "Rossetti", "123456789", 45, TCC
Aline = Docentes::Professor.new 21000.71, "Aline", "987655555", 38, TCC
Regina = Docentes::Professor.new 871253.52, "Irineu", "33333311111", 60, GAN

include BD
a = BD::BD.new
b = BD::BD.new
c = BD::BD.new

a.salvar Rossetti
b.salvar Aline
c.salvar Regina